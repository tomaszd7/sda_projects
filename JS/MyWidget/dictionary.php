<?php

$dictionary = [
    'kon' => 'duze zwierze',
    'konik' => 'mniejszy kon',
    'konio' => 'frontmen Bigcyca',
    'koniuszy' => 'chłop od konia',
    'koniec' => 'koniec konia czyli zad'    
];

$search = trim($_GET['search']);
$result = [];

if (!empty($search)) {
    foreach ($dictionary as $key => $value) {
        if (strpos($key, $search) !== false) {
            $result[$key] = $value;
        }
    }
    
}


echo json_encode($result);