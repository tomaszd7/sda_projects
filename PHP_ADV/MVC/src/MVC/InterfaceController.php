<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MVC;

/**
 * Description of InterfaceController
 *
 * @author diabelek
 */
interface InterfaceController {           
    public function renderPage();
}
