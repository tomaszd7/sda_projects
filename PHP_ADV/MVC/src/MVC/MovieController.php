<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MainController
 *
 * @author diabelek
 */
namespace MVC;

use MVC\Model\Movie;

class MovieController extends AbstractController {
    private $id;

    public function __construct($twig, $id) {
        parent::__construct($twig);
        
        $this->id = $id;
    }

    public function renderPage()
    {
        $movie = new Movie();
        
        return $this->twig->render('edit-page.twig', [
            'movie' => $movie->getMovie($this->id)
        ]);
    }
    
    public function changeMovie($title, $duration)
    {
        if (isset($_FILES['image'])) {
            
        }
        
        if (!empty($title) && !empty($duration)) {
            $movie = new Movie();
            $movie->updateMovie($this->id, $title, $duration);
        }
        
        return $this->renderPage();
    }
    
    public function removeMovie()
    {
        $movie = new Movie();
        $movie->removeMovie($this->id);
        
        $mainPage = new MainController($this->twig);
        return $mainPage->renderPage();
    }
}
