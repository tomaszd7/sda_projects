<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MVC\Model;

/**
 * Description of Movie
 *
 * @author diabelek
 */
class Movie extends AbstractModel {
    public function getMovie($id)
    {
        $result = $this->conn->query('SELECT m_id as id, m_name as name,'
                . ' m_duration as duration FROM movie WHERE m_id = ' . $id);
        
        return $result->fetch_assoc();
    }
    
    public function updateMovie($id, $title, $duration)
    {
        $this->conn->query('UPDATE movie SET m_name = "' . $title . '", '
            . ' m_duration = ' . $duration . ' WHERE m_id = ' . $id);
    }
    
    public function removeMovie($id)
    {
        $this->conn->query('DELETE FROM movie WHERE m_id = ' . $id);
    }
}
