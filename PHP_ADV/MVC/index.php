<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once  __DIR__ . '/vendor/autoload.php';
require_once  __DIR__ . '/const.php';

use MVC\MainController;
use MVC\MovieController;

$app = new \Silex\Application();

$app->register(new \Silex\Provider\TwigServiceProvider(), [
        'twig.path' => __DIR__ . '/view',
]);

$app->get('/', function() use ($app) {
    $controller = new MainController($app['twig']);
    return $controller->renderPage();
});

$app->get('/sort/{field}/{order}', function($field, $order) use ($app) {
    $controller = new MainController($app['twig']);
    return $controller->renderPage($field, $order);
});

$app->get('/edit/{id}', function($id) use ($app) {
    $controller = new MovieController($app['twig'], $id);
    return $controller->renderPage();
});

$app->get('/remove/{id}', function($id) use ($app) {
    $controller = new MovieController($app['twig'], $id);
    return $controller->removeMovie();
});

$app->post('/edit/{id}', function($id) use ($app) {
    $controller = new MovieController($app['twig'], $id);
    
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $duration = filter_input(INPUT_POST, 'duration', FILTER_SANITIZE_STRING);

    return $controller->changeMovie($title, $duration);
});

$app->run();