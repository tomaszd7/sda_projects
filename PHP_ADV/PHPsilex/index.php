<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once  __DIR__ . '/vendor/autoload.php';

$app = new \Silex\Application();

$app->register(new \Silex\Provider\TwigServiceProvider(), [
        'twig.path' => __DIR__ . '/view',
]);

$app->get('/', function () use ($app) {
    $nc = new MyNamespace\NewClass();
    return $nc->none();
    
    
});

$app->get('/test/test/test', function () use ($app) {
    return $app['twig']->render('test.twig', [
        'test' => 'MOJA WARTOSC',
        'test2' => 'MOJA WARTOSC2',
        'test3' => 'MOJA WARTOSC3'
        
        
        ]);
    
    
});

$app->get('/form', function () use ($app) {
    return $app['twig']->render('form.twig.html', [
      
        ]);
     
});

$app->post('/form', function () use ($app) {
    $_POST['field'];
    return $app['twig']->render('response.twig.html', [
        'name' => $_POST['field']
      
        ]);
     
});

$app->get('/test', function () use ($app) {
    $nc = new MyNamespace\AbcClass();
    return $nc->none();
    
    
});

$app->get('/test2', function () use ($app) {
    $nc = new MyNamespace\Test\NewTest2();
    return $nc->none();
    
    
});


$app->get('/{var1}/{var1}/{id}', function ($var1, $var2, $id) use ($app) {
    $nc = new MyNamespace\Test\NewTest2();
    return $id; //  $id->none();
    
    
});

$app->get('/test_twig', function () use ($app) {
    $nc = new MyNamespace\TestTwig();
    return   $nc->none();
    
    
});

$app->run();
