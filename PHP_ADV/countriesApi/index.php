<?php
//header('Content-Type: text/plain');

include_once './CountriesApi.php';

if (isset($_GET['name'])) {
    $ctr = $_GET['name'];
} else {
    $ctr = null;
}

$api = new CountriesApi($ctr);
if (isset($ctr)) {
    $results = $api->getSingleCountry($ctr);
} else {
    $results = $api->getCountriesByRegions();
}

?>

<html>
    <style>
        div {
            display: inline-table;
            width: 33%;
        }
    </style>
    
    <body>
        
        <?php
        if (isset($ctr)) {
            
            foreach ($results as $name => $info) {
            print_r($name . '=>' . $info . '<br>');
                
            }
            
        } else {
            foreach ($results as $region => $countries) { ?>
            <div>
                <h2>REGION: <?php echo $region; ?></h2>
                <?php 
                foreach ($countries as $country) {
                    echo '<a href="index.php?name=' . $country . '">' . $country. '</a><br>';
                } ?>                            
            </div>
            <?php             
                }         
            }?>
        
        
        
<!--        <script>
        var highest = 0;
        var divs = document.getElementsByTagName('div');
        for (var i = 0; i < divs.length; i++) {
            if (divs[i].clientHeight > highest) {
                highest = divs[i].clientHeight;
            }
        }
        console.log(highest);
        for (var i = 0; i < divs.length; i++) {
            divs[i].style.height =  highest;        // tak nie dziala                  
        }
        </script>-->
        
        
    </body>
        
</html>
    
    

