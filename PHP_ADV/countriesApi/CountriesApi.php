<?php

class CountriesApi {
    
    private $url = 'https://restcountries.eu/rest/v1/all';
    private $countries;
    private $url_ctr1 = 'https://restcountries.eu/rest/v1/name/';
    private $url_ctr2 = '?fullText=true';    
    
    
    public function __construct($ctr) {
        if (!isset($ctr)) {          
            $temp = file_get_contents($this->url);
            $this->countries = json_decode($temp, true);                          
        }                
    }
    
    public function getSingleCountry($country) {
        $url = $this->url_ctr1 . $country . $this->url_ctr2;
        
        $temp = json_decode(file_get_contents($url), true);
//        print_r($temp); 
        return [
            'name' => $temp[0]['name'],
            'capital' => $temp[0]['capital'],
            'nativeName' => $temp[0]['nativeName'],
            'population' => $temp[0]['population']            
            ];
    }
    
    
    
    public function getAllCountries() {
        $temp = [];
        foreach ($this->countries as $country) {
            array_push($temp, $country['name']);
        }        
        return $temp;
    }
    
    public function getCountriesByRegions() {
        $temp =[];
        foreach ($this->countries as $country) {
            // in array moznabylo
            if (!isset($temp[$country['region']])) {
                $temp[$country['region']] = [];
            }
            array_push($temp[$country['region']], $country['name']);            
        }
        return $temp;                
    }
    
    
}


// obiekt dostep do pol $obiekt->name
// tablica assoc to $tablica['name']
