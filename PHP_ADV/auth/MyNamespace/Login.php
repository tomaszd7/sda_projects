<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MyNamespace;

class Login {

    private $jsonAccess;
    private $userJson;
    private $email;
    private $password;

    public function __construct() {
        $this->jsonAccess = new JsonAccess();
        
        $this->validateInputs();
    }

    /* Walidacja pól formularza */
    private function validateInputs() {
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $this->password = $password;
        $this->email = $email;

        if (!empty($email) and ! empty($password)) {
            
        } else {
            array_push($this->errorsList, "Pola mają niepoprawny format");
        }
    }

    public function checkUser() {
        /* Zczytanie listy użytkowników z "users.json" */
        $this->userJson = $this->jsonAccess->readJson();
        /* Pętla po wszystkich użytkownikach */
        foreach ($this->userJson as $user) {

            /* Jeśli użytkownik jest taki sam i hasło jest takie samo
             * to dodaj nazwę użytkownika do sesji i zwróć true
             */
            if ($user['email'] === $this->email and $user['password'] === Auth::sha1($this->password)) {
                Session::saveName($this->email);
                return true;
            }
        }
        return false;
    }
     public function getEmail() {
        return $this->email;
    }
}
