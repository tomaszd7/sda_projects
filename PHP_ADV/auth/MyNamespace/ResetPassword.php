<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MyNamespace;

class ResetPassword {

    private $errorsList = [];
    private $email;
    
    private $userJson=[];
    private $jsonAccess;
    public function __construct() {
        $this->jsonAccess= new JsonAccess();
        /* Zczytywanie jsona "users.json" */
        $this->userJson = $this->jsonAccess->readJson();
      
        $this->validateInputs();
    }

    private function validateInputs() {
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        if (!empty($email)) {
            $changed = false;
            foreach ($this->userJson as $user) {
                if ($user['email'] === $email) {
                    $this->resetPassword($email);
                    $changed = true;
                    break;
                }
            }
            if (!$changed) {
                array_push($this->errorsList, "Nie no... stare hasło nie bangla!");
            }
        } else {
            array_push($this->errorsList, "Podaj email");
        }
    }

    /* Sprawdzenie czy jest jakikolwiek bład w wysłanych danych */
    public function isAnyInputError() {
        return (bool) count($this->errorsList);
    }

    /* Pobranie listy błędów, może być pusta */
    public function getInputErrors() {
        return $this->errorsList;
    }

    public function getEmail() {
        return $this->email;
    }

    private function resetPassword($email)
    {
        foreach ($this->userJson as $key => $user) {
            if ($user['email'] === $email) {
                $randomHash = md5(rand(0, 100000) . 'sdthsfgvsuvwc5454v7hebt');
                $this->userJson[$key]['reset-hash'] = $randomHash;
                break;
            }
        }
        
        $this->jsonAccess->saveJson($this->userJson);
        
        $mail = new \PHPMailer();
        
//        $mail->SMTPDebug = 3;  
        
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;          
        $mail->SMTPSecure = "tls"; 
        $mail->Username = 'jsphppoz1mail@gmail.com';                 
        $mail->Password = 'Test12345';
        $mail->Port = 587;

        $mail->addAddress('diabelek1@gmail.com');     
        $mail->isHTML(true);
        
        $mail->Subject = 'Przypomnienie hasła';
        $mail->Body    = 'This is the HTML message body <b>in bold!</b>';

        $mail->send();
    }
}