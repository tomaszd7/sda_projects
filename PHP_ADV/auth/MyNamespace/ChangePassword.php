<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MyNamespace;

class ChangePassword {

    private $errorsList = [];
    private $email;
    
    private $userJson=[];
    private $jsonAccess;
    public function __construct() {
        $this->jsonAccess= new JsonAccess();
        /* Zczytywanie jsona "users.json" */
        $this->userJson = $this->jsonAccess->readJson();
      
        $this->validateInputs();
    }
//2 porownac
// spradzic hasla 
    
    private function validateInputs() {
        if ($email = Session::getName()) {
                        
            $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
            $passwordConfirm = filter_input(INPUT_POST, 'password-confirm', FILTER_SANITIZE_STRING);
            $passwordOld = filter_input(INPUT_POST, 'password-old', FILTER_SANITIZE_STRING);

            $this->email = $email;

            /* Walidacja inputów */
            if (!empty($password) && ! empty($passwordConfirm) && !empty($passwordOld)) {
                if ($password === $passwordConfirm) {
                    if($this->checkUserPassword($passwordOld)) {
                        
                        $this->changePassword($password);
                    }
                    
                } else {
                    array_push($this->errorsList, "Hasła nie są takie same");
                }
            } else {
                array_push($this->errorsList, "Pola mają niepoprawny format");
            }
        } else{
            array_push($this->errorsList, "Brak emaila w sesji");
        }
        
    }
    
    private function changePassword($password) {
        $changed =false;
        
        foreach ($this->userJson as $key => $user) {
            if ($this->email === $user['email']) {                
                $this->userJson[$key]['password'] = Auth::sha1($password);
                $changed = true;
                break;
            }                        
        }      
        if ($changed){            
            $this->jsonAccess->saveJson($this->userJson);                
        } else {
            array_push($this->errorsList, "Stare haslo niepoprawne ");
        }
    }

    /* Sprawdzenie czy jest jakikolwiek bład w wysłanych danych */
    public function isAnyInputError() {
        return (bool) count($this->errorsList);
    }

    /* Pobranie listy błędów, może być pusta */
    public function getInputErrors() {
        return $this->errorsList;
    }

    public function getEmail() {
        return $this->email;
    }

    /* Sprawdzenie czy użytkownik istnieje w tablicy użytkowników
     * (wziętej z pliku users.json)
     */
    private function checkUserPassword($passwordOld) {
        foreach ($this->userJson as $user) {

            if ($this->email === $user['email'] && $user['password'] === Auth::sha1($passwordOld)) {
                return true;
            }
        }
        return false;
    }



}
