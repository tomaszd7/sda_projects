<?php

namespace MyNamespace;

class JsonAccess {

    private $userJson=[];
    private $jsonPath = 'resources/users.json';

    public function __construct($path = null) {
        if ($path !== null) {
            $this->jsonPath = $path;
        }
        $this->setJson();
    }

    /* Zczytanie jsona */
    private function setJson() {

        $this->userJson = (array)json_decode(
                file_get_contents($this->jsonPath), true);
    }

    /* Zapis jsona */
    public function saveJson($users) {
       
        file_put_contents($this->jsonPath, json_encode($users));
        
    }

    /* Zwrócenie zawartości jsona */
    public function readJson() {

        return $this->userJson;
    }

}
