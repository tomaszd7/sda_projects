<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MyNamespace;

class Register {

    private $errorsList = [];
    private $email;
    
    private $userJson=[];
    private $jsonAccess;
    public function __construct() {
        $this->jsonAccess= new JsonAccess();
        /* Zczytywanie jsona "users.json" */
        $this->userJson = $this->jsonAccess->readJson();
      
        $this->validateInputs();
    }

    private function validateInputs() {
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $passwordConfirm = filter_input(INPUT_POST, 'password-confirm', FILTER_SANITIZE_STRING);

        $this->email = $email;

        /* Walidacja inputów */
        if (!empty($email) and ! empty($password) and ! empty($passwordConfirm)) {
            if ($password === $passwordConfirm) {
                $this->registerUser($email, $password);
            } else {
                array_push($this->errorsList, "Hasła nie są takie same");
            }
        } else {
            array_push($this->errorsList, "Pola mają niepoprawny format");
        }
    }

    /* Sprawdzenie czy jest jakikolwiek bład w wysłanych danych */
    public function isAnyInputError() {
        return (bool) count($this->errorsList);
    }

    /* Pobranie listy błędów, może być pusta */
    public function getInputErrors() {
        return $this->errorsList;
    }

    public function getEmail() {
        return $this->email;
    }

    /* Sprawdzenie czy użytkownik istnieje w tablicy użytkowników
     * (wziętej z pliku users.json)
     */
    private function userExists($email) {
        foreach ($this->userJson as $user) {

            if ($email === $user['email']) {
                return true;
            }
        }
        return false;
    }


    /* Dodanie użytkownika do tablicy -> jsona */
    private function registerUser($email, $password) {
        if (!$this->userExists($email)) {
           
            /* Dodanie użytkownika do tablicy */
            array_push($this->userJson, [
                'email' => $email,
                /* wyliczenie hashu hasła za pomocą klasy Auth z użyciem soli */
                'password' => Auth::sha1($password)
            ]);

           /* Zapis tablicy do jsona */
           $this->jsonAccess->saveJson($this->userJson);
        }
    }

}
