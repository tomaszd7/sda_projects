<?php

namespace MyNamespace;

class Auth {

    private static $salt = 'vjiuhwz847h7878';

    public static function sha1($password) {
        return sha1($password . self::$salt);
    }
}
