<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

/* Include autoloada */
require_once  __DIR__ . '/vendor/autoload.php';
/* Zainicjowanie sesji */
$Session = new MyNamespace\Session();
$app = new \Silex\Application();

/* Ustawienie ścieżki do plików twig */
$app->register(new \Silex\Provider\TwigServiceProvider(), [
        'twig.path' => __DIR__ . '/view',
]);


$app->get('/', function () use ($app) {
    return 'OK';
});

/* WIDOK REJESTRACJI */
$app->get('/register', function () use ($app) {   
    return $app['twig']->render('register.twig');
});

/* WIDOK LOGOWANIA */
$app->get('/login', function () use ($app) {   
    return $app['twig']->render('login.twig');
});

/* WIDOK ZMIANY HASŁA */
$app->get('/change-password', function () use ($app) {   
    return $app['twig']->render('change-password.twig');
});

/* WIDOK PANELU UŻYTKOWNIKA */
$app->get('/dashboard', function () use ($app) {   
    return $app['twig']->render('dashboard.twig',[
        'email' => MyNamespace\Session::getName()
    ]);
});


$app->get('/reset-password', function () use ($app) {   
    return $app['twig']->render('reset-password.twig');
});


$app->get('/reset-password-confirmation/{email}/{hash}', function ($email, $hash) use ($app) {   
    
    $passwordConfirm = new \MyNamespace\ResetPasswordConfirmation();
    
    
    return $app['twig']->render('reset-password-confirmation.twig');
});


$app->post('/login', function () use ($app) {   
    
    $login = new \MyNamespace\Login();
    
    /* Sprawdzamy czy użytkownik podał poprawny login i hasło */
    if($login->checkUser()){
        /* Jeśli podał poprawne to przekierowanie na dashboard */
      return $app->redirect('dashboard');;
    }
    return $app['twig']->render('login.twig',[
        'email' => $login->getEmail()
    ]);
});

$app->post('/register', function () use ($app) {   
    $register = new \MyNamespace\Register();
    $errors = [];
    
    /* Jeśli jest jakikolwiek błąd w formularzu */
    if($register->isAnyInputError()) {
        /* Do zmiennej przypisujemy błedy */
        $errors = $register->getInputErrors();
    }
    return $app['twig']->render('register.twig', [
        /* Błędy zwracamy do twiga */
        'errors' => $errors,
        'email' => $register->getEmail()
    ]);
});


$app->post('/change-password', function () use ($app) {   
    $changePassword = new \MyNamespace\ChangePassword();
    $errors = [];
    
    /* Jeśli jest jakikolwiek błąd w formularzu */
    if($changePassword->isAnyInputError()) {
        /* Do zmiennej przypisujemy błedy */
        $errors = $changePassword->getInputErrors();
    }
    return $app['twig']->render('change-password.twig', [
        /* Błędy zwracamy do twiga */
        'errors' => $errors,
        'email' => $changePassword->getEmail()
    ]);
});


$app->post('/reset-password', function () use ($app) {   
    $resetPassword = new \MyNamespace\ResetPassword();
    $errors = [];
    
    /* Jeśli jest jakikolwiek błąd w formularzu */
    if($resetPassword->isAnyInputError()) {
        /* Do zmiennej przypisujemy błedy */
        $errors = $resetPassword->getInputErrors();
    }
    return $app['twig']->render('reset-password.twig', [
        /* Błędy zwracamy do twiga */
        'errors' => $errors,
        'email' => $resetPassword->getEmail()
    ]);
});

$app->run();