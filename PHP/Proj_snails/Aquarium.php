<?php

require_once 'Snails.php';

class HomeAquarium {
    protected $cost = 100;
    protected $capacity = 50;
    protected $maxCapacity = 100;
    protected $deathMultiplier = 1;
    protected $maxDeathMultiplier = 1.2;
    private $snails = [];
    private $container = 0;
    private $growFuncCount = 0;
    
    public function __construct($snailsCount, $death_rate) {
        $this->deathMultiplier = $death_rate;
        $this->maxDeathMultiplier = $death_rate;
        for ($i = 0; $i < $snailsCount; $i++) {
            $this->addSnail(new CommonSnail);
        }
    }


    public function addSnail(CommonSnail $snail) { // typowanie zmiennej
        if ($this->maxCapacity > count($this->snails)) {
            $this->snails[] = $snail;
        }
    }
    
    public function getSnailsCount() {
        return count($this->snails);
    }
    
    public function grow() { // cykl zycia akwarium 
        $this->growFuncCount++;
        $matureCount = 0;
//        for ($i = 0; $i < count($this->snails); $i++) { // zamienione na forach choc zostaja zmienne 
        
        if ($this->capacity < count($this->snails)) {
            $multiplier = $this->maxDeathMultiplier;
        } else {
            $multiplier = $this->deathMultiplier;
        }
        
        
        foreach ($this->snails as $i => $value) {            
            // slimaki rosna z roznym modyfikatorem srodowiska 
            $this->snails[$i]->grow($multiplier);  
         
            // zbieram glutenium 
            $this->container += $this->snails[$i]->harvest();
            // czy slimak zyje i moze rozmnazac  
            if($this->snails[$i]->getIsMature()) {
                $matureCount++;
            }
            // wywalam usmiercone 
            if (!$value->getIsAlive()) {
                unset($this->snails[$i]); // tak mozna bo nie wywali to foreach 
            }
        }
        
        if ($matureCount > 1) {
            $newSnailsCount = intval(rand(1, 3)*0.2*count($this->snails)) + 2;
            for ($i = 0; $i < $newSnailsCount; $i++) {
                $this->addSnail(new CommonSnail());
            }
        }
//        print_r( $this->growFuncCount . chr(9) . count($this->snails) . '<br/>');
    }
    
    public function collectGlutenium() {
        $tmp = $this->container;
        $this->container = 0;
        return $tmp;
    }
    
}
