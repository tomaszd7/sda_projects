<?php

include_once 'Site.php';
include_once 'Aquarium.php';

Site::head();

$snailsCount = 0;  
$deathRate = 10;

// nowa gra 
if (isset($_POST['snails'])) {
    $snailsCount = intval($_POST['snails']); 
    setcookie('snails', $snailsCount);
    if (isset($_POST['death_rate'])) {
       $deathRate = intval($_POST['death_rate']); 
    }
    setcookie('death_rate', $deathRate);
    $aquarium = new HomeAquarium($snailsCount, $deathRate);
    
} else { 
    // juz jest gra 
    if (isset($_COOKIE['snails'])) {
        $snailsCount = $_COOKIE['snails'];
        $deathRate = $_COOKIE['death_rate'];
        
        // tu GROW??
        if (isset($_POST['grow'])) {
           $aquarium = new HomeAquarium($snailsCount, $deathRate);
           $aquarium->grow();
           $snailsCount = $aquarium->getSnailsCount();
           setcookie('snails', $snailsCount);
        }
    } 
}

Site::body($snailsCount, $deathRate);
Site::footer($snailsCount, $deathRate);

// za kazdym razem musi przeladowac strone 