<?php

class CommonSnail {
    protected $cost = 0.25;
    protected $lifeSpan = null;
    protected $mature = null;
    protected $suddenDeath = 0.01; 
    private $age = 0;
    private $isAlive = true;
    private $gluteniumContainer = 0;
    protected $minProductivity = 100;
    protected $maxProductivity = 200;
    
    public function __construct() {
        $this->lifeSpan = rand(15, 18);
        $this->mature = 1;
    }
    
    public function getIsAlive() {
        return $this->isAlive;// moge odczytac ta zmienna ale niemoge jest zmienic z akwarium 
    }
    
    public function getIsMature() {
        return $this->isAlive && $this->age >= $this->mature; // zwraca true or false 
    }


    public function grow($deathModify = 1) {
        $this->age++;
        // smierc ze starosci 
        if ($this->age > $this->lifeSpan) {
            $this->isAlive = false;
        }
        // losowa smiereltnosc
        if ($deathModify * $this->suddenDeath > $temp = rand(1,100000) / 100000) { 
        // // tu ze ponizej 0.001 bo losuje miedzy 0 a 1 wiec bedzie 1 tysieczna
            $this->isAlive = false;
        }
        // co roku slimak ma inna losowa produktywnosc 
        if ($this->getIsMature()) { // to fajne!
            $this->gluteniumContainer += rand($this->minProductivity, $this->maxProductivity) / 10000;
        }
    }
    
    public function getInfo() {
        echo 'Snail stats: <br/>';
        echo 'Age: ' . $this->age . '<br/>';
    }
    
    public function harvest() {
        $tmp = $this->gluteniumContainer;
        $this->gluteniumContainer = 0;
        return $tmp;
    }
}


class NaturalisSnail extends CommonSnail{
    protected $cost = 0.35;
    protected $suddenDeath = 0.002; 
    protected $minProductivity = 200; // bo to pole musi byc przekazane parentowi bo inaczej wezmie z CommonSnail
    protected $maxProductivity = 400;
    // a private bylaby tylko w tej klasie - nawet jesli jest dzieckiem 
    
    public function __construct() {
        $this->lifeSpan = rand(10, 12);
        $this->mature = rand(3, 4);
    }
}


class TechnologicusSnail extends CommonSnail{
    protected $cost = 0.50;
    protected $suddenDeath = 0.005; 
    protected $minProductivity = 800;
    protected $maxProductivity = 1000;
    
    public function __construct() {
        $this->lifeSpan = rand(5, 7);
        $this->mature = rand(1, 2);
    }
}

