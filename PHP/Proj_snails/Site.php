
<?php

/**
 * Description of Site
 *
 * @author Python
 */
class Site {
    
    public static function head() {
        ?>
        <html>
            <head>
                <title>Snails</title>
            </head>
            <body>
        <?php        
    }
    
    public static function body($snailsCount, $deathRate) {
        ?>
            <div class="menu" style="border: 1px solid black; width:280px;">
                <form action="" method="POST">
                    Snails: <input name="snails" value="<?php echo $snailsCount;?>"/>
                    <input type="submit" value="START"/><br/>
                    Death rate: <input name="death_rate" value="<?php echo $deathRate;?>"/>
                </form>
                <form action="" method="POST">
                    <input type="hidden" name="grow" />
                    <input type="submit" value="GROW"/>
                </form>
            </div>
            <div class="game" style="width:280px; height: 300px; border:1px solid black;">
                  <?php
                  for ($i = 0; $i< $snailsCount; $i++) {                                            
                      ?>
                <div style="background-color: black; margin: 1px; width:20px; height:20px; display: inline-block;">                    
                </div>
                    <?php
                  }
                  ?>                  
            </div>        
        <?php
    }
    
    public static function footer($snailsCount, $deathRate) {
//        echo '<br/> Snails: ' . $snailsCount . ' DeathRate ' . $deathRate;
        ?>
            </body>
        </html>
        <?php
    }    
}
