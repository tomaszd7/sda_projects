<?php

class Apifinal {
    private $url = 'http://api.openweathermap.org/data/2.5/';
    private $apiId = '623ab05272f04dfb30f338568094b48b';
    private $weather;
    private $forecast;

    public function __construct($city) {
        //London,uk
        $this->weather = $this->getUrlData('weather', $city);
        $this->forecast = $this->getUrlData('forecast', $city); //upraszczamy zapis poprzez wrzucenie powatarzajacego kodu do funkcji 
    }
    
    private function getUrlData($type, $city) {
        return json_decode(file_get_contents($this->url . $type . '?q=' . $city . '&appid=' . $this->apiId . '&units=metric&lang=pl'), true);
    }

    
    public function getCurrentWeather() {
        return [
          'name' =>  $this->weather['name'],
          'temp' =>  $this->weather['main']['temp'],
          'pressure' =>  $this->weather['main']['pressure'],
          'humidity' =>  $this->weather['main']['humidity']
        ];
    }

    public function getForecast() {
        $result = [];
        $tempAvr = 0;
        $pressAvr =0;

        foreach ($this->forecast['list'] as $key => $value) {
            $date = date('d.m.Y H:i', $value['dt']);
//            $date = $value['dt_txt'];
            $temp = $value['main']['temp'];
            $press = $value['main']['pressure'];
            $tempAvr += $temp;
            $pressAvr += $press;
            $result[$date] = [ 'temp' =>$temp, 
                'press' => $press];            
        }
        $tempAvr /= count($this->forecast['list']);
        $pressAvr /= count($this->forecast['list']);
        
        return [ 
            'days' =>$result,
            'avg' => [
                'temp' => $tempAvr ,
                'press' => $pressAvr]
                ];
    }
    
    public function getClouds() {
        $clouds = $this->weather['weather'][0]['main'];
        return $clouds;
    }    

}
