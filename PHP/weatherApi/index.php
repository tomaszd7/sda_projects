<?php
include_once 'Apifinal.php';

$cities = [
    'Poznan',
    'Warsaw',
    'Berlin',
    'Tokyo',
    'Chicago',
    'Sao Paulo',
    'New York'
];


//$citiesToHide = json_decode($_COOKIE['citiesToHide']);
//$citiesToHide[] = $_GET['city'] ;
//setcookie('citiesToHide', json_encode($citiesToHide));


// setcookie - tworzy fizycznie ciastko - ale nie updaetuje zmiennej $_COOKIE ... 
if (isset($_COOKIE['citiesToHide'])) {
    $citiesToHide = json_decode($_COOKIE['citiesToHide'], true);        
} else {
    $citiesToHide = []; 
}

// usuwanie z tablicy 
if (isset($_GET['unhide']) && in_array($_GET['unhide'], $citiesToHide)) {
    foreach ($citiesToHide as $key => $value) {
        if ($_GET['unhide'] === $value) {
            unset($citiesToHide[$key]); /// TAK SIE USUWA!!!!!!!
            break;
        }
    }
    $citiesToHide[] = $_GET['city']; // nie musimy dodac do $_COOKIE bo bedzie sprawdzac zmienna $citiesToHide
}

if (isset($_GET['city']) && !in_array($_GET['city'], $citiesToHide)) {
    $citiesToHide[] = $_GET['city']; // nie musimy dodac do $_COOKIE bo bedzie sprawdzac zmienna $citiesToHide
}
        
if (count($citiesToHide)) { // czyli true czyli wieksza niz 0 
    setcookie('citiesToHide', json_encode($citiesToHide));
}

// dodawanie miast do ulubionych 
if (isset($_COOKIE['citiesToAdd'])) {
    $citiesToAdd = json_decode($_COOKIE['citiesToAdd']);        
} else {
    $citiesToAdd = []; 
}
if (isset($_GET['cityToAdd']) && !in_array($_GET['cityToAdd'], $citiesToAdd)) {
    $citiesToAdd[] = $_GET['cityToAdd']; // nie musimy dodac do $_COOKIE bo bedzie sprawdzac zmienna $citiesToHide
}
        
if (count($citiesToAdd)) { // czyli true czyli wieksza niz 0 
    setcookie('citiesToAdd', json_encode($citiesToAdd));
}

$cities = array_merge($cities, $citiesToAdd);

?>

<html>
    <head>   
        <title>Test na API</title>
        <style>
            section {
                display: inline-block;
                width: 40%;
                margin: 1% 4%;
                border: 1px solid black;
            }
            a {
                text-decoration: none;
            }
            a:hover {
                background-color: lightskyblue;
            }
        </style>
    </head>    
    <body><!-- tak wciecia dla kazdej SEKCJI !!! -->
        
        <form action="forecast.php"> <!--to wywoluje forecast z parametrem!! inny plik -->
            <input type="text" name="city">
            <input type="submit" value="Szukaj">
            
        </form>
        
        
       <?php foreach ($cities as $city) {    
            if (!in_array($city, $citiesToHide)) {
           ?>
           <section>
                <?php 
                $api = new Apifinal($city);
                $data = $api->getCurrentWeather();
                ?>
                Miasto: <?php echo $data['name']; ?> </br> 
                Temperatura: <?php echo $data['temp']; ?></br>
                Ciśnienie: <?php echo $data['pressure']; ?></br>
                Wilgotnosc: <?php echo $data['humidity']; ?> </br>
                <a href="forecast.php?city=<?php echo $data['name']; ?>">Prognoza pogody</a>
                <a href="?city=<?php echo $data['name']; ?>">-X-</a>
                
            </section>
       <?php }else { ?>
        <a href="index.php?unhide=<?php echo $city; ?>">Pokaz miasto</a>
       <?php } }?>
    </body>
</html>