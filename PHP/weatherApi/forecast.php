<?php
include_once 'Apifinal.php';

if(!empty($_GET['city'])) {
    $api = new Apifinal($_GET['city']);
    $data = $api->getForecast(); ?>

<html>
    <head>   
        <title>Test na API</title>

    </head>    
    <body>
        <a href="index.php?cityToAdd=<?php echo $_GET['city']; ?>">Dodaj miasto</a>
        <?php foreach ($data['days'] as $key => $value) { ?>
            <section>
                Data: <?php echo $key; ?> </br> 
                Temperatura: <?php echo $value['temp']; ?></br>
                Ciśnienie: <?php echo $value['press']; ?>  </br> 
            </section>
        <?php } ?>
        Średnia temperatura:  <?php echo $data['avg']['temp']; ?></br> 
        Średnie ciśnienie: <?php echo $data['avg']['press']; ?></br> 
    </body>
</html>
<?php } ?>
