-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 13 Wrz 2016, 19:58
-- Wersja serwera: 10.1.13-MariaDB
-- Wersja PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test2`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `director`
--

CREATE TABLE `director` (
  `dir_id` int(11) NOT NULL,
  `dir_director` varchar(255) DEFAULT NULL,
  `dir_name` varchar(255) NOT NULL,
  `dir_surname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `director`
--

INSERT INTO `director` (`dir_id`, `dir_director`, `dir_name`, `dir_surname`) VALUES
(1, 'Steven Spielberg', 'Steven', 'Spielberg'),
(2, 'Clint Eastwood', 'Clint', 'Eastwood'),
(3, 'Tom Hanks ', 'Tom', 'Hanks'),
(4, 'Martin Scorsese ', 'Martin', 'Scorsese'),
(5, 'Christopher Nolan ', 'Christopher', 'Nolan'),
(6, 'Ridley Scott', 'Ridley', 'Scott'),
(7, 'Quentin Tarantino', 'Quentin', 'Tarantino'),
(8, 'Michael Mann', 'Michael', 'Mann'),
(9, ' James Cameron', 'James', 'Cameron');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `genre`
--

CREATE TABLE `genre` (
  `g_id` int(11) NOT NULL,
  `g_genre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `genre`
--

INSERT INTO `genre` (`g_id`, `g_genre`) VALUES
(1, 'si-fi'),
(2, 'horror'),
(3, 'adventure'),
(4, 'action'),
(5, 'fantasy'),
(6, 'familly'),
(7, 'comedy'),
(8, 'dramat'),
(9, 'history'),
(10, 'disaster');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `medium`
--

CREATE TABLE `medium` (
  `med_id` int(11) NOT NULL,
  `med_medium` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `medium`
--

INSERT INTO `medium` (`med_id`, `med_medium`) VALUES
(1, 'cd'),
(2, 'dvd'),
(3, 'video tape'),
(4, 'blueray'),
(5, 'pendrive');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `movie`
--

CREATE TABLE `movie` (
  `m_id` int(11) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `m_duration` int(11) DEFAULT NULL,
  `m_genre_id` int(11) DEFAULT NULL,
  `m_yearp_id` int(11) DEFAULT NULL,
  `m_medium_id` int(11) DEFAULT NULL,
  `m_director_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `movie`
--

INSERT INTO `movie` (`m_id`, `m_name`, `m_duration`, `m_genre_id`, `m_yearp_id`, `m_medium_id`, `m_director_id`) VALUES
(2, 'star wars', 120, 1, 8, 4, 3),
(3, 'xmen', 120, 1, 2, 4, 2),
(4, 'big children', 100, 3, 4, 1, 1),
(5, 'flash', 140, 4, 5, 4, 3),
(6, 'obssesion', 110, 3, 6, 1, 3),
(7, 'second war', 80, 5, 7, 3, 5),
(8, 'lowrider', 120, 10, 8, 4, 5),
(9, 'broken heard', 90, 6, 10, 1, 4),
(10, 'milioner', 120, 9, 8, 4, 4),
(11, 'jak wytresowac smoka', 75, 7, 9, 2, 6),
(12, 'transformers', 90, 8, 4, 3, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `production_year`
--

CREATE TABLE `production_year` (
  `p_id` int(11) NOT NULL,
  `p_yearp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `production_year`
--

INSERT INTO `production_year` (`p_id`, `p_yearp`) VALUES
(2, 1999),
(3, 2000),
(4, 2001),
(5, 2002),
(6, 2003),
(7, 2004),
(8, 2005),
(9, 2006),
(10, 2007),
(11, 2008),
(12, 2009),
(13, 2010),
(14, 2011),
(15, 2012),
(16, 2013),
(17, 2014),
(18, 2015),
(19, 2016);

--
-- Indeksy dla zrzut�w tabel
--

--
-- Indexes for table `director`
--
ALTER TABLE `director`
  ADD PRIMARY KEY (`dir_id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`g_id`);

--
-- Indexes for table `medium`
--
ALTER TABLE `medium`
  ADD PRIMARY KEY (`med_id`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `production_year`
--
ALTER TABLE `production_year`
  ADD PRIMARY KEY (`p_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `director`
--
ALTER TABLE `director`
  MODIFY `dir_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `genre`
--
ALTER TABLE `genre`
  MODIFY `g_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `medium`
--
ALTER TABLE `medium`
  MODIFY `med_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `movie`
--
ALTER TABLE `movie`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `production_year`
--
ALTER TABLE `production_year`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;