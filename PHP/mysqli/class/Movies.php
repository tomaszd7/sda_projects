<?php

class Movies {
    private $conn;
    private $result;
    private $names = [];
    private $edit;


    public function __construct() {
        
        // podlaczenie sie do bazy danych  host user password baza 
        $this->conn = new mysqli('localhost:3307', 'root', '', 'movies' );

        //var_dump($conn->connect_errno);
        
        if (isset($_GET['eid'])) {
            $dataE = $this->conn->query('select * from movie where m_id = '.$_GET['eid']);
            $this->setEdit($dataE);
        }
        
        if (isset($_GET['titleE']) && isset($_GET['durationE']) && isset($_GET['idE'])) {
//            die('UPDATE movie SET m_name = "'. $_GET['titleE'].'", '
//                  .'m_duration = '. $_GET['durationE']. ' WHERE m_id = ' . $_GET['idE'] );
            $dataU = $this->conn->query('UPDATE movie SET m_name = "'. $_GET['titleE'].'", '
                  .'m_duration = '. $_GET['durationE']. ' WHERE m_id = ' . $_GET['idE'] );
            header('Location: index.php');
        }
        
        if (isset($_GET['id'])) {
            
            $this->conn->query('DELETE FROM movie WHERE m_id = '.$_GET['id']);
        }
        
        if (isset($_GET['title']) && isset($_GET['duration'])) {
//            die('INSERT INTO movie (m_name, m_duration) VALUES' 
//                    .'("'.$_GET['title'].'","'.$_GET['duration'].'")');
            $this->conn->query('INSERT INTO movie (m_name, m_duration) VALUES' 
                    .'("'.$_GET['title'].'", '.$_GET['duration'].')');
            header('Location: index.php');
        }

        
        $order = isset($_GET['sort']) ? $_GET['sort'] : 'm_name';
        $direction = 'ASC';
        //select 
        
        $this->result = $this->conn->query('SELECT * FROM movie '
                . 'LEFT JOIN production_year ON m_yearp_id = p_id '
                . 'ORDER BY '. $order . ' '.$direction);
        
        // to zwraca specjalny typ obiektu ktory ma rozne klucze do pobrania danych 
        //var_dump($result);

        //object(mysqli_result)#2 (5) 
        //{ ["current_field"]=> int(0) ["field_count"]=> int(6) ["lengths"]=> NULL ["num_rows"]=> int(11) ["type"]=> int(0) }

//        $result->fetch_assoc(); // tu pobiera pierwszy rekord z wynikow zapytania 

        // to jest fany sposob sprawdzenia true/false przyprzypisaniu do zmiennej !!!!!
        
        $this->setNames();
    }
    
    private function setEdit ($dataE) {
        while ($result = $dataE->fetch_assoc()) {
        //    var_dump($data);
            $this->edit = $result;
        }    
        $this->edit['id'] = $_GET['eid'];
        
    }
        
    private function setNames() {            
        while ($data = $this->result->fetch_assoc()) {
        //    var_dump($data);
            $this->names[] = [
                'm_name' => $data['m_name'],
                'm_duration' => $data['m_duration'],
                'p_yearp' => $data['p_yearp'],
                'm_id' => $data['m_id']
            ];
        }    
    }
    
    public function getNames() {
        return $this->names;
    }
    public function getEdit() {
        return $this->edit;
    }
}
