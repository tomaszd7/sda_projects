<?php
include_once 'class/Movies.php';


$app = new Movies();

$names = $app->getNames();

//var_dump($names);
?>

<html>
    <body>
        <form>
            Nazwa<input type="text" name="title">
            Czas<input type="text" name="duration">
            <input type="submit" value="Ślij">
        </form>
        
        <table>
            <thead>
            <th>
                <a href="index.php?sort=m_name">NAZWA</a>
            </th>
            <th>
                <a href="index.php?sort=m_duration">CZAS</a>
            </th>
            <th>
                <a href="index.php?sort=p_yearp">ROK</a>
            </th>
            <th>
                AKCJE
            </th>                            
            </thead>
            <tbody>
                <?php for ($i = 0; $i < count($names); $i++) { ?>
                    <tr>
                        <td>
                            <?php echo $names[$i]['m_name']; ?>
                        </td>                    
                        <td>
                            <?php echo $names[$i]['m_duration']; ?>
                        </td>                    
                        <td>
                            <?php echo $names[$i]['p_yearp']; ?>
                        </td>  
                        <td>                            
                            <a href="index.php?id=<?php echo $names[$i]['m_id']; ?>">USUN</a>
                        </td>
                        <td>                            
                            <a href="edit.php?eid=<?php echo $names[$i]['m_id']; ?>">EDYTUJ</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </body>
</html>