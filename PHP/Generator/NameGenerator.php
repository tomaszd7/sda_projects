<?php

class NameGenerator {

    //put your code here
    private $persons = [
        'Krzysztof',
        'Maciej',
        'Bartek',
        'Tomek D',
        'Tomek N',
        'Mateusz',
        'Piotr',
        'Danuta',
        'Dawid',
        'Jacek',
        'Łukasz',
        'Rafał'
    ];
    private $randomIndex;

    public function __construct() {
        session_start();
        if (empty($_SESSION['persons'])) {
            $_SESSION['persons'] = [];  
        }
        if (count($_SESSION['persons']) !== count($this->persons)) {
            $this->randomIndex = $this->getRandomName();
            $this->addName();
        } else {
            $_SESSION['persons'] = [];
        }
    }

    private function getRandomName() {
        do {
            $index = array_rand($this->persons);
        } while (array_key_exists($index, $_SESSION['persons']));
        return $index;
    }

    private function addName() {
        $_SESSION['persons'][$this->randomIndex] = $this->persons[$this->randomIndex];
    }
    
    public function getResult() {
        return $_SESSION['persons'];
    }
}
