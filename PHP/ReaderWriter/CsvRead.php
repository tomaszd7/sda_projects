<?php

class CsvRead extends Reader {
    
    private $result;
    
    public function getResults()
    {
        $this->result = parent::getData();
        
        return $this->result;
    }
}