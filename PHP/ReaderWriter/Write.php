<?php

class Write {
    
    private $fileName;
    private $fileData;

    public function writeFile()
    {
        file_put_contents($this->fileName, $this->fileData);
    }
    
    public function setFile($fileName)
    {
        $this->fileName = $fileName;
    }
    
    public function setFileData($fileData)
    {
        $this->fileData = $fileData;
    }
}
