<?php

require_once 'Write.php';
require_once 'XmlWrite.php';
require_once 'CsvWrite.php';
require_once 'JsonWrite.php';

$xmlWrite = new XmlWrite();
$xmlWrite->setFile('newdata.xml');
$xmlWrite->setFileData('<?xml version="1.0" encoding="UTF-8"?><catalog>1</catalog></xml>');
$xmlWrite->writeFile();


$csvWrite = new CsvWrite();
$csvWrite->setFile('newdata.csv');
$csvWrite->setFileData('1,"Eldon Base for stackable storage shelf, platinum",Muhammed MacIntyre,3,-213.25,38.94,35,Nunavut,Storage & Organization,0.8');
$csvWrite->writeFile();

$jsonWrite = new CsvWrite();
$jsonWrite->setFile('newdata.json');
$jsonWrite->setFileData('{
    "data": {
        "packageType": {
            "id": "packageType",
            "name": "Rodzaj wyjazdu",
            "type": "enum"}}}');
$jsonWrite->writeFile();



//require_once 'Reader.php';
//require_once 'XmlRead.php';
//require_once 'JsonRead.php';
//require_once 'CsvRead.php';
//
//$xmlRead = new XmlRead();
//$xmlRead->setFile('data.xml');
//$xmlRead->setFileExtension('xml');
//$xmlResult = $xmlRead->getResults();


//$jsonRead = new JsonRead();
//$jsonRead->setFile('destinations.json');
//$jsonResult = $jsonRead->getResults();

//
//$csvRead = new CsvRead();
//$csvRead->setFile('data.csv');
//$csvResult = $csvRead->getResults();


//echo '<pre>';
//print_r($xmlResult);
//echo '</pre>';
////
//echo '<pre>';
//print_r($jsonResult);
//echo '</pre>';
////
//echo '<pre>';
//print_r($csvResult);
//echo '</pre>';