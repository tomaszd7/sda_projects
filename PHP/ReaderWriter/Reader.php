<?php

class Reader {
    
    private $fileName;
    private $fileData;
    private $fileExtension;

    public function readFile()
    {
        $this->fileData = file_get_contents($this->fileName);
    }
    
    public function setFile($fileName)
    {
        $this->fileName = $fileName;
    }
    
    public function getData()
    {
        if($this->checkExtension()) {
            $this->readFile();
            return $this->fileData;
        }
        
        return null;
    }
    
    public function getFileName()
    {
        return $this->fileName;
    }
    public function setFileExtension($fileExtension)
    {
        $this->fileExtension = $fileExtension;
    }
    
    public function checkExtension()
    {
        // Metoda 1 - szukanie ciągu znaków
//        if (strpos(parent::getFileName(), '.xml') !== false) {
//            return true;
//        }
        
        // Metoda 2 - explode
        
        $items = explode('.', $this->getFileName());
        
        if(array_pop($items) === $this->fileExtension){
            return true;
        }
         
        return false;
    }
}
