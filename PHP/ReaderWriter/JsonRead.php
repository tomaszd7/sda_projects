<?php

class JsonRead extends Reader {
    
    private $result;
    
    public function getResults()
    {
        $this->result = json_decode(parent::getData(), true);
        
        return $this->result;
    }
}
